package main

import "strconv"
import "sort"

func ReturnInt() int {
    return 1
}

func ReturnFloat() float32 {
    return 1.1
}

func ReturnIntArray() [3]int {
    ret := [...]int{1, 3, 4}
    return ret
}

func ReturnIntSlice() []int {
    ret := []int{1, 2, 3}
    return ret
}

func IntSliceToString(a []int) string {
    ret := ""

    for _, val := range a {
        ret += strconv.Itoa(val)
    }

    return ret
}

func MergeSlices(a []float32, b []int32) []int {
    ret := make([]int, len(a) + len(b))

    ind := 0
    for _, val := range a {
        ret[ind] = int(val)
        ind++
    }

    for _, val := range b {
        ret[ind] = int(val)
        ind++
    }

    return ret
}

func GetMapValuesSortedByKey(input map[int]string) []string {
    ret := make([]string, 0, len(input))

    keys := make([]int, 0, len(input))
    for k := range input {
        keys = append(keys, k)
    }

    sort.Ints(keys)
    for _, k := range keys {
        ret = append(ret, input[k])
    }

    return ret
}
